### Task 5: Pushing the Docker Image to Docker Hub

#### Objective
Learn how to tag and push your Docker image to Docker Hub, a cloud-based registry service that allows you to share your Docker images with others. This task is crucial for distributing Docker images and making them available for deployment on different systems or for collaborative projects.

#### Prerequisites
- A Docker image of your Flask application, built as per the previous steps.
- A Docker Hub account. If you don't have one, you can sign up at [Docker Hub](https://hub.docker.com/).

#### Steps

1. **Log in to Docker Hub from the Command Line**
   - Open your terminal or command prompt.
   - Log in to your Docker Hub account using the command:
     ```
     docker login
     ```
   - Enter your Docker Hub username and password when prompted.

2. **Tag Your Docker Image**
   - Before pushing the image to Docker Hub, you need to tag it with your Docker Hub username.
   - The general format for tagging is:
     ```
     docker tag [EXISTING_IMAGE_NAME]:[TAG] [DOCKER_HUB_USERNAME]/[NEW_IMAGE_NAME]:[TAG]
     ```
   - For example:
     ```
     docker tag my-flask-app:v1 myusername/my-flask-app:v1
     ```
   - Replace `myusername` with your Docker Hub username. The `EXISTING_IMAGE_NAME` and `TAG` should match the name and tag you used when building the image.

3. **Push the Image to Docker Hub**
   - Once the image is tagged, you can push it to Docker Hub using the `docker push` command:
     ```
     docker push myusername/my-flask-app:v1
     ```
   - This command uploads your image to your Docker Hub repository.

4. **Verify the Image on Docker Hub**
   - Go to [Docker Hub](https://hub.docker.com/) and log in to your account.
   - Navigate to your repositories. You should see the `my-flask-app` repository with the `v1` tag.
   - This means your image is now hosted on Docker Hub and can be pulled from any machine with Docker installed.

#### Outcome
By completing this task, you have successfully pushed your Docker image to Docker Hub, making it accessible for others to pull and use. This is a fundamental step in sharing Docker images and collaborating on Docker-based projects.

#### Next Steps
You can now pull and run your Docker image on any system with Docker installed, using the command `docker pull myusername/my-flask-app:v1`. This is particularly useful for deploying applications across different environments or sharing your work with colleagues.
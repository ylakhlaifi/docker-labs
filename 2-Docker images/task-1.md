### Creating a Simple Flask Application

In this part of the lab, you'll create a basic Flask web application. Flask is a lightweight WSGI web application framework in Python, ideal for getting started with web development and easily containerized with Docker.

#### Prerequisites
- Basic knowledge of Python.
- Python installed on your machine (preferably Python 3).
- A text editor or IDE for writing Python code.

#### Steps

1. **Set Up Your Project Directory**
   - Create a new directory on your computer for your Flask project. You can name it something like `flask-docker-app`.
   - Navigate into this directory.

2. **Create the Flask Application**
   - Inside your project directory, create a new file named `app.py`. This file will contain your Flask application code.
   - Open `app.py` in your text editor or IDE.

3. **Write Your Flask Application Code**
   - Add the following Python code to `app.py`:
     ```python
     from flask import Flask
     app = Flask(__name__)

     @app.route('/')
     def hello_world():
         return 'Hello, Docker!'

     if __name__ == '__main__':
         app.run(debug=True, host='0.0.0.0')
     ```
   - This code creates a basic Flask application with a single route (`/`) that returns the text "Hello, Docker!".

4. **Create a Requirements File**
   - Flask applications depend on external libraries. You need to specify these dependencies in a `requirements.txt` file.
   - Create a new file named `requirements.txt` in the same directory as `app.py`.
   - Add the following line to `requirements.txt`:
     ```
     Flask
     ```
   - This line tells Python to install Flask, which is the only dependency for this simple application.

5. **Testing Your Flask Application Locally (Optional)**
   - Before containerizing your application, you can test it locally.
   - Make sure Flask is installed in your environment:
     ```
     pip install Flask
     ```
   - Run your application:
     ```
     python app.py
     ```
   - Open a web browser and navigate to `http://localhost:5000`. You should see "Hello, Docker!" displayed.

#### Outcome
You have now set up a basic Flask application ready for containerization with Docker. The next step will be to write a Dockerfile to define how your Flask application will be packaged into a Docker image.

#### Next Steps
Proceed to write the Dockerfile, which will instruct Docker on how to build an image for your Flask application. This image can then be run as a container on any machine with Docker installed, without the need for a Python environment or Flask installed locally.



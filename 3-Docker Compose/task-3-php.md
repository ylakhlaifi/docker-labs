### Enhancing the Lab: Adding a PHP Script to Test the Database

To further enhance the Docker Compose training lab, we'll add a simple PHP script to test the MySQL database. This script will be served by the Nginx server. We'll also modify the Docker Compose setup to include PHP processing capabilities and mount the script into the Nginx container.

#### Objective
Learn how to integrate a PHP script into the Nginx server to interact with the MySQL database. This task demonstrates how to extend the functionality of Docker containers using volumes and custom configurations.

#### Prerequisites
- Basic knowledge of PHP and MySQL.
- Docker and Docker Compose installed on your machine.

#### Task: Integrating a PHP Script with Nginx and MySQL

1. **Creating the PHP Script**
   - Create a new file named `test_db.php` in your project directory.
   - Add the following PHP code, which connects to MySQL and displays a success message:
     ```php
     <?php
     $host = 'db';  // Name of the MySQL service in the Docker Compose file
     $user = 'root';
     $pass = 'mypassword';  // Same as in your Docker Compose file
     $conn = new mysqli($host, $user, $pass);

     if ($conn->connect_error) {
         die("Connection failed: " . $conn->connect_error);
     } 
     echo "Connected successfully to MySQL!";
     ?>
     ```

2. **Modifying the Docker Compose File**
   - You need to modify the Nginx service to handle PHP scripts. This typically involves using a PHP-FPM (FastCGI Process Manager) container.
   - Update your `docker-compose.yml` file to include a PHP-FPM service and mount the directory containing your PHP script into both the Nginx and PHP-FPM containers:
     ```yaml
     version: '3'
     services:
       web:
         image: nginx
         ports:
           - "8080:80"
         volumes:
           - ./html:/usr/share/nginx/html  # Mounting the directory containing PHP script
       php:
         image: php:fpm
         volumes:
           - ./html:/usr/share/nginx/html
       jenkins:
         image: jenkins/jenkins
         ports:
           - "8081:8080"
       db:
         image: mysql:5.7
         environment:
           MYSQL_ROOT_PASSWORD: mypassword
         ports:
           - "3306:3306"
     ```
   - Create a directory named `html` in your project directory and move `test_db.php` into this directory.

3. **Configuring Nginx to Handle PHP**
   - You'll need to configure Nginx to process PHP files using PHP-FPM. This involves customizing the Nginx configuration.
   - Create a custom Nginx configuration file (e.g., `default.conf`) in your project directory with the following content:
     ```
     server {
         listen 80;
         server_name localhost;

         root /usr/share/nginx/html;
         index index.php index.html index.htm;

         location / {
             try_files $uri $uri/ =404;
         }

         location ~ \.php$ {
             try_files $uri =404;
             fastcgi_pass php:9000;
             fastcgi_index index.php;
             fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
             include fastcgi_params;
         }
     }
     ```
   - Mount this configuration file into the Nginx container by adding the following line under the `volumes` section of the `web` service in your `docker-compose.yml`:
     ```yaml
         volumes:
           - ./html:/usr/share/nginx/html
           - ./default.conf:/etc/nginx/conf.d/default.conf
     ```

4. **Running the Updated Docker Compose Setup**
   - Run `docker-compose up -d` again to apply the changes and start the updated services.

5. **Testing the PHP Script**
   - Open a web browser and navigate to `http://localhost:8080/test_db.php`.
   - You should see a message indicating a successful connection to MySQL.

#### Outcome
By completing this task, you have integrated a PHP script into your Docker Compose setup, demonstrating how to use Nginx with PHP-FPM to serve dynamic content and interact with a MySQL database.

#### Next Steps
Explore further by adding more complex PHP scripts, connecting to the database, and performing CRUD operations. This will provide a more comprehensive understanding of developing and deploying web applications using Docker.
### Task 4: Exploring Docker Compose Commands

#### Objective
Familiarize yourself with various Docker Compose commands to manage and interact with the services defined in your `docker-compose.yml` file. This task will cover essential commands for controlling and monitoring Nginx, Jenkins, PHP-FPM, and MySQL services.

#### Instructions

1. **Listing Running Services**
   - To see a list of all running services defined by your Docker Compose file, use:
     ```
     docker-compose ps
     ```
   - This command shows the status of each container, including names, command, state, and ports.

2. **Viewing Logs of Services**
   - To check the logs for a specific service, use:
     ```
     docker-compose logs [service_name]
     ```
   - Replace `[service_name]` with `web`, `jenkins`, `php`, or `db` to view the logs for Nginx, Jenkins, PHP-FPM, or MySQL, respectively.
   - For continuous log output, add the `-f` flag (follow mode).

3. **Stopping and Starting Services**
   - To stop all services without removing them, use:
     ```
     docker-compose stop
     ```
   - To start the services again, use:
     ```
     docker-compose start
     ```
   - You can also stop and start individual services by specifying the service name.

4. **Restarting Services**
   - To restart all or a specific service, use:
     ```
     docker-compose restart [service_name]
     ```
   - Omit `[service_name]` to restart all services.

5. **Scaling Services**
   - If you want to scale a stateless service (like the web service), use:
     ```
     docker-compose up -d --scale web=3
     ```
   - This command scales the Nginx service to run 3 instances.

6. **Removing Services**
   - To stop and remove all containers, networks, and volumes created by `docker-compose up`, use:
     ```
     docker-compose down
     ```
   - Add the `--volumes` flag to also remove the volumes.

7. **Pulling Service Images**
   - To pull the latest images for all services, use:
     ```
     docker-compose pull
     ```
   - This is useful to ensure you're using the latest versions of the images specified in your `docker-compose.yml`.

#### Outcome
By completing this task, you have gained hands-on experience with various Docker Compose commands essential for managing and monitoring multi-container applications.

#### Next Steps
Experiment with these commands to get a better understanding of how they affect your Docker environment. Try modifying the `docker-compose.yml` file and observe how these changes impact the behavior of your services when you apply them using Docker Compose commands.
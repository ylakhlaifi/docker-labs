### Task 2: Running the Services with Docker Compose

#### Objective
Learn how to use Docker Compose to start and manage the multi-container application defined in your `docker-compose.yml` file, including Nginx, Jenkins, and MySQL services.

#### Instructions

1. **Navigating to Your Docker Compose Directory**
   - Open your command-line interface (CLI).
   - Navigate to the directory where your `docker-compose.yml` file is located.

2. **Starting the Services**
   - Run the following command to start all services defined in your Docker Compose file:
     ```
     docker-compose up -d
     ```
   - The `-d` flag runs the containers in detached mode, meaning they run in the background.
   - Docker Compose will read your `docker-compose.yml` file, pull the necessary images (if they're not already downloaded), and start the containers.

3. **Verifying the Containers are Running**
   - To ensure that all the containers are up and running, use:
     ```
     docker-compose ps
     ```
   - This command lists all the containers managed by the Docker Compose file in the current directory. You should see your Nginx, Jenkins, and MySQL containers listed.

4. **Checking Container Logs (Optional)**
   - If you want to check the logs for a specific service, use:
     ```
     docker-compose logs [service_name]
     ```
   - Replace `[service_name]` with `web`, `jenkins`, or `db` to view the logs for Nginx, Jenkins, or MySQL, respectively.

5. **Accessing the Running Services**
   - **Nginx**: Open a web browser and navigate to `http://localhost:8080`. You should see the Nginx welcome page.
   - **Jenkins**: Go to `http://localhost:8081`. You'll be greeted with the Jenkins setup wizard.
   - **MySQL**: Connect to your MySQL database using a database client at `localhost:3306` with the root password set in the Docker Compose file (`mypassword`).

#### Outcome
By completing this task, you have successfully used Docker Compose to start a multi-container application. You've also learned how to verify that the services are running and access them through their respective ports.

#### Next Steps
In the next tasks, you will explore managing these services, including stopping, starting, and scaling, as well as updating configurations and applying those changes.
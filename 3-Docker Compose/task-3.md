### Task 3: Accessing and Testing the Services

#### Objective
Learn how to interact with and test each service (Nginx, Jenkins, and MySQL) running in your Docker Compose environment. This task focuses on ensuring that each service is accessible and functioning as expected.

#### Instructions

1. **Accessing the Nginx Service**
   - Open a web browser.
   - Navigate to `http://localhost:8080`.
   - You should see the Nginx welcome page, indicating that the Nginx server is running and accessible.

2. **Setting Up and Accessing Jenkins**
   - In your web browser, go to `http://localhost:8081`.
   - You will be greeted with the Jenkins setup wizard.
   - Follow the initial setup steps for Jenkins:
     - Retrieve the initial admin password. You can get this from the Jenkins container logs:
       ```
       docker-compose logs jenkins
       ```
     - Look for a line in the logs that contains the initial admin password and enter it in the setup wizard.
     - Install the suggested plugins and create an admin user as prompted by the Jenkins setup.

3. **Connecting to the MySQL Database**
   - Use a database client tool of your choice that can connect to MySQL (e.g., MySQL Workbench, phpMyAdmin, or even command-line MySQL client).
   - Set up a new connection with the following details:
     - **Hostname**: `localhost`
     - **Port**: `3306`
     - **User**: `root`
     - **Password**: `mypassword` (as set in your Docker Compose file)
   - Test the connection to ensure you can connect to the MySQL database running in the Docker container.

4. **Verifying Service Interaction (Optional)**
   - If you have a web application or a sample PHP script, you can deploy it to the Nginx server to test the interaction between the web server and the MySQL database.
   - This step might require additional configuration, such as setting up a PHP interpreter with Nginx or adjusting Nginx configuration files.

#### Outcome
By completing this task, you have verified that each service defined in your Docker Compose setup is accessible and functioning. You've accessed the Nginx web server, set up Jenkins, and connected to the MySQL database.

#### Next Steps
In the next tasks, you will explore more advanced Docker Compose functionalities, such as managing services, scaling, and updating configurations.
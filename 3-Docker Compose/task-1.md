### Task 1: Creating the Docker Compose File

#### Objective
Learn to create a `docker-compose.yml` file that defines a multi-container application with Nginx as a web server, Jenkins for CI/CD, and MySQL as a database.

#### Instructions

1. **Setting Up Your Workspace**
   - Create a new directory on your computer where you will store your Docker Compose file and any related configuration files.
   - Open this directory in a text editor or Integrated Development Environment (IDE) of your choice.

2. **Understanding the Docker Compose File Structure**
   - Docker Compose files are written in YAML format.
   - The file starts with a version declaration, which defines the format version of the file. We'll use version 3, a commonly used version.
   - Services are defined under the `services` key. Each service represents a container.

3. **Creating the Docker Compose File**
   - In your directory, create a file named `docker-compose.yml`.
   - Open `docker-compose.yml` in your text editor.

4. **Defining the Nginx Service**
   - Start by defining the Nginx service. This service will use the official Nginx image from Docker Hub.
   - Map port 8080 on your host to port 80 on the Nginx container to access the web server externally.
   - Add the following content to your `docker-compose.yml`:
     ```yaml
     version: '3'
     services:
       web:
         image: nginx
         ports:
           - "8080:80"
     ```

5. **Adding the Jenkins Service**
   - Next, add the Jenkins service. Jenkins will be used for CI/CD processes.
   - Use the official Jenkins image and map port 8081 on your host to port 8080 on the Jenkins container.
   - Add the following under the `services` section:
     ```yaml
       jenkins:
         image: jenkins/jenkins
         ports:
           - "8081:8080"
     ```

6. **Configuring the MySQL Service**
   - Finally, add the MySQL service. This will be your database server.
   - Use the MySQL 5.7 image and set a root password (required for MySQL to start) through environment variables.
   - Map port 3306 (default MySQL port) on your host to port 3306 on the MySQL container.
   - Add this configuration under the `services` section:
     ```yaml
       db:
         image: mysql:5.7
         environment:
           MYSQL_ROOT_PASSWORD: mypassword
         ports:
           - "3306:3306"
     ```

7. **Reviewing the Complete Docker Compose File**
   - Your final `docker-compose.yml` should look like this:
     ```yaml
     version: '3'
     services:
       web:
         image: nginx
         ports:
           - "8080:80"
       jenkins:
         image: jenkins/jenkins
         ports:
           - "8081:8080"
       db:
         image: mysql:5.7
         environment:
           MYSQL_ROOT_PASSWORD: mypassword
         ports:
           - "3306:3306"
     ```

#### Outcome
By completing this task, you have created a Docker Compose file that defines a multi-container application with Nginx, Jenkins, and MySQL. Each service in the file is configured to run in its container with specific port mappings and settings.

#### Next Steps
Proceed to Task 2, where you will use this Docker Compose file to run your multi-container application.
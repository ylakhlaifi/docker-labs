### Task 5: Modifying and Scaling Services

#### Objective
Learn how to modify service configurations in the Docker Compose file and apply these changes. Additionally, understand how to scale stateless services to handle increased load.

#### Instructions

1. **Modifying Service Configuration**
   - Open your `docker-compose.yml` file in a text editor.
   - Make a change to one of the services. For example, you could change the Nginx service to map a different port on your host machine:
     ```yaml
     web:
       image: nginx
       ports:
         - "8082:80"  # Changed from 8080 to 8082
     ```
   - Save the changes.

2. **Applying Configuration Changes**
   - To apply your changes, navigate to the directory containing your `docker-compose.yml` file and run:
     ```
     docker-compose up -d
     ```
   - Docker Compose will stop and recreate the containers that have changed configurations. In this case, it will update the Nginx container to reflect the new port mapping.

3. **Verifying the Changes**
   - Verify that the changes have been applied by accessing the modified service. For the example change above, you would access the Nginx service at `http://localhost:8082`.

4. **Scaling Stateless Services**
   - Docker Compose allows you to scale stateless services easily. For instance, if you want to scale the Nginx service to run 3 instances, use:
     ```
     docker-compose up -d --scale web=3
     ```
   - This command increases the number of Nginx instances to 3. Note that scaling is more effective for stateless services like web servers.

5. **Observing the Scaled Services**
   - Use `docker-compose ps` to see the multiple instances of the Nginx service running.
   - Note that all instances are accessible through the same port on the host machine, as Docker handles the load balancing internally.

6. **Understanding Limitations**
   - Be aware that not all services can be scaled in this manner. Stateful services like databases (MySQL in this case) usually require more complex strategies for scaling and redundancy.

#### Outcome
By completing this task, you have learned how to modify service configurations in Docker Compose and apply these changes. You also understand how to scale stateless services to handle increased load.

#### Next Steps
Experiment with different configurations and scaling strategies. Try adding new services or changing existing ones to see how Docker Compose manages these updates. This experience is valuable for managing real-world applications using Docker.
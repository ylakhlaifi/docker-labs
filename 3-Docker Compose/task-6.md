### Task 6: Cleanup

#### Objective
Learn how to clean up your Docker Compose environment by stopping and removing containers, networks, and optionally, volumes created during the lab. This task is essential for maintaining a tidy and efficient Docker setup, especially after testing or development work.

#### Instructions

1. **Stopping Containers**
   - If you have any running containers from your Docker Compose setup, stop them using:
     ```
     docker-compose stop
     ```
   - This command stops all the running containers defined in your `docker-compose.yml` file without removing them.

2. **Removing Containers and Networks**
   - To remove the containers along with the default network created by Docker Compose, use:
     ```
     docker-compose down
     ```
   - This command stops (if they are still running) and removes the containers, default network, and the associated resources created by `docker-compose up`.

3. **Removing Volumes (Optional)**
   - If you created any volumes (either named or anonymous) and wish to remove them, add the `--volumes` flag:
     ```
     docker-compose down --volumes
     ```
   - This will remove all volumes declared in the `volumes` section of your Docker Compose file. Be cautious with this command as it will result in data loss from the volumes.

4. **Removing Images (Optional)**
   - If you want to remove the Docker images used in the lab (like Nginx, Jenkins, MySQL), you can do so using Docker commands. For example:
     ```
     docker rmi nginx jenkins/jenkins mysql:5.7
     ```
   - Ensure no containers are using these images before attempting to remove them.

5. **Pruning System-Wide (Optional)**
   - Docker provides a way to clean up unused containers, networks, images, and optionally, volumes system-wide. Use with caution:
     ```
     docker system prune
     ```
   - To include volumes in the cleanup, use:
     ```
     docker system prune --volumes
     ```

#### Outcome
By completing this task, you have learned how to effectively clean up your Docker Compose environment, which is crucial for managing resources and maintaining system performance.

#### Next Steps
With your Docker environment cleaned up, you can move on to more advanced Docker tasks or experiments, knowing how to maintain a clean setup. This skill is particularly important in environments where Docker is used extensively for development, testing, or production deployments.
### Task 4: Planning and Applying Configuration

#### Objective
Learn how to use Terraform's `plan` and `apply` commands to preview and apply changes to your infrastructure as defined in your Terraform configuration.

#### Prerequisites
- An initialized Terraform working directory (completed in Task 3).
- A Terraform configuration file (`main.tf`) in your directory.

#### Steps

1. **Planning Infrastructure Changes**
   - Open a terminal and navigate to your Terraform project directory.
   - Run the `terraform plan` command:
     ```bash
     terraform plan
     ```
   - This command performs a dry run, showing you what changes Terraform will make to your infrastructure without actually applying them.
   - Review the output of the plan command. It will list all the actions Terraform will perform (create, update, destroy) on your infrastructure.

2. **Understanding the Plan Output**
   - The plan output is Terraform's way of showing the difference between your current infrastructure state (as known by Terraform) and the state described in your configuration files.
   - It's crucial to carefully review this output to ensure that the changes it proposes are expected and safe to apply.

3. **Applying the Configuration**
   - If you're satisfied with the plan, apply the configuration with:
     ```bash
     terraform apply
     ```
   - Terraform will show the plan again and prompt you for confirmation before proceeding.
   - Type `yes` and press Enter to confirm and start the provisioning process.
   - Terraform will then apply the configuration, creating, updating, or destroying infrastructure resources as needed.

4. **Review Apply Output**
   - As Terraform applies the configuration, it provides real-time feedback about what it's doing.
   - Once complete, Terraform will display a summary of the actions it took.

5. **Verify Infrastructure**
   - After Terraform successfully applies your configuration, you can verify the changes directly in your cloud provider's console (e.g., AWS Management Console).
   - Ensure that the resources have been created or modified as expected.

#### Outcome
By completing this task, you will have successfully planned and applied a Terraform configuration, resulting in the creation, modification, or destruction of resources in your cloud environment according to your defined infrastructure as code.

#### Best Practices
- Always run `terraform plan` before `terraform apply` to understand the changes that will be made.
- Regularly commit your Terraform configuration files to version control to track changes and collaborate with others.

#### Next Steps
Experiment with modifying your Terraform configuration and re-running `plan` and `apply` to see how Terraform manages changes to infrastructure. As you become more comfortable, you can explore more complex configurations and Terraform modules.
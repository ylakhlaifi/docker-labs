### Setting Up Terraform on Ubuntu and Amazon Linux

Terraform can be easily installed on both Ubuntu and Amazon Linux. Below are step-by-step instructions for each.

#### Setting Up Terraform on Ubuntu

1. **Update and Install Required Software**
   - Open a terminal.
   - Update your package list:
     ```bash
     sudo apt-get update
     ```
   - Install `wget` and `unzip` if they are not already installed:
     ```bash
     sudo apt-get install -y wget unzip
     ```

2. **Download Terraform**
   - Go to [Terraform's official download page](https://www.terraform.io/downloads.html) to find the appropriate package for your system.
   - Alternatively, you can use `wget` to download Terraform directly from the command line. Replace `[VERSION]` with the desired version:
     ```bash
     wget https://releases.hashicorp.com/terraform/[VERSION]/terraform_[VERSION]_linux_amd64.zip
     ```
   - For example, to download version 1.0.0, you would use:
     ```bash
     wget https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_amd64.zip
     ```

3. **Install Terraform**
   - Unzip the downloaded file:
     ```bash
     unzip terraform_[VERSION]_linux_amd64.zip
     ```
   - Move the Terraform binary to a directory included in your system's PATH, such as `/usr/local/bin`:
     ```bash
     sudo mv terraform /usr/local/bin/
     ```
   - Verify the installation:
     ```bash
     terraform -v
     ```

#### Setting Up Terraform on Amazon Linux

1. **Update System**
   - Open a terminal.
   - Update your system:
     ```bash
     sudo yum update -y
     ```

2. **Install `wget` and `unzip`**
   - Install necessary tools:
     ```bash
     sudo yum install -y wget unzip
     ```

3. **Download and Install Terraform**
   - Follow the same steps as for Ubuntu, starting from downloading Terraform using `wget`.

#### Post-Installation Steps

- **Verify Installation:**
  - After installation, you can verify it by running:
    ```bash
    terraform -v
    ```
  This command should return the Terraform version, indicating that it has been installed successfully.

- **Configure Auto-Completion (Optional):**
  - Terraform offers an auto-completion feature for commands. You can enable it by running:
    ```bash
    terraform -install-autocomplete
    ```
  - Restart your terminal for the changes to take effect.

#### Conclusion

You now have Terraform installed on both Ubuntu and Amazon Linux. The next step is to configure Terraform to work with your cloud provider, such as AWS, by setting up the necessary credentials and writing Terraform configuration files.
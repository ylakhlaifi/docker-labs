### Task 5: Modifying Infrastructure

#### Objective
Learn how to modify your infrastructure using Terraform by changing the configuration files and applying those changes. This task demonstrates Terraform's ability to manage infrastructure changes efficiently and safely.

#### Prerequisites
- An initialized and previously applied Terraform configuration (from Tasks 3 and 4).
- A basic understanding of how to write Terraform configuration files.

#### Steps

1. **Modify Your Terraform Configuration**
   - Open your Terraform configuration file (`main.tf`) in a text editor.
   - Make a change to your infrastructure. For example, if you have an AWS EC2 instance, you might change the instance type or add tags.
   - Example modification (changing instance type):
     ```hcl
     resource "aws_instance" "iac_example" {
       ami           = "ami-0c55b159cbfafe1f0"
       instance_type = "t2.small"  # Changed from t2.micro to t2.small
       tags = {
         Name = "iac-modified-instance"
       }
     }
     ```
   - Save the changes to your file.

2. **Plan the Changes**
   - Run `terraform plan` in your project directory.
   - Review the output of the plan to understand the changes Terraform will make. Ensure that the changes match your expectations.

3. **Apply the Changes**
   - If you're satisfied with the plan, apply the changes with:
     ```bash
     terraform apply
     ```
   - Terraform will show the plan again and ask for confirmation.
   - Type `yes` to proceed. Terraform will apply the changes to your infrastructure.

4. **Review the Apply Output**
   - Terraform provides feedback about the changes being made.
   - Once complete, Terraform will display a summary of the actions it took.

5. **Verify the Changes in the Cloud Provider**
   - Check your cloud provider's console (e.g., AWS Management Console) to confirm that the changes have been applied as expected.

#### Outcome
You have successfully modified your infrastructure using Terraform. This task demonstrates Terraform's ability to manage incremental changes to infrastructure, ensuring that your environment evolves safely and predictably over time.

#### Best Practices
- Always review the plan output carefully before applying changes.
- Incrementally apply changes and verify each step to ensure infrastructure stability.
- Keep your Terraform configurations version-controlled to track changes and facilitate collaboration.

#### Next Steps
Experiment with more complex changes and different types of resources. As you grow more comfortable with Terraform, you can explore advanced features like modules, functions, and remote backends.
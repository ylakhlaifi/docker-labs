### Task 3: Running a Docker Container

#### Objective
Learn how to create and run a Docker container using the Nginx image you've pulled. This task will demonstrate the basics of container creation and management.

#### Instructions

1. **Open Command-Line Interface**
   - Ensure your command-line interface (CLI) is open. This could be Terminal on macOS and Linux or Command Prompt or PowerShell on Windows.

2. **Run a Docker Container Using the Nginx Image**
   - To start a new container from the Nginx image, use the following command:
     ```
     docker run --name my-nginx -d -p 8080:80 nginx
     ```
   - Here's what each part of the command means:
     - `--name my-nginx`: Assigns the name "my-nginx" to your container.
     - `-d`: Runs the container in detached mode, meaning it runs in the background.
     - `-p 8080:80`: Maps port 8080 on your host to port 80 on the container. This allows you to access the Nginx server via port 8080 on your local machine.
     - `nginx`: Specifies the image to use (the Nginx image you pulled earlier).

3. **Verify the Container is Running**
   - Check if your container is running by executing:
     ```
     docker ps
     ```
   - You should see your container "my-nginx" in the list. Note details like container ID, status, and ports.

4. **Accessing the Nginx Server**
   - Open a web browser and go to `http://localhost:8080`.
   - You should see the default Nginx welcome page, confirming that your container is running and serving content.

5. **Inspecting the Running Container (Optional)**
   - If you want to inspect the running container in more detail, use:
     ```
     docker inspect my-nginx
     ```
   - This command provides detailed information about the container's configuration, network settings, mount points, and more.

#### Outcome
By completing this task, you have successfully run an Nginx server inside a Docker container and accessed it via your web browser. This demonstrates the fundamental process of running and accessing services in Docker containers.

#### Next Steps
In the next task, you will learn how to manage Docker containers, including stopping and removing them.
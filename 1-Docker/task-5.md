### Task 5: Stopping and Removing Containers

#### Objective
Learn how to stop and remove Docker containers. This task is crucial for managing the lifecycle of containers and maintaining a clean, efficient Docker environment.

#### Instructions

1. **Stopping a Docker Container**
   - To stop a running container, you need its name or container ID. You can find this information by running `docker ps`.
   - Use the following command to stop the container:
     ```
     docker stop my-nginx
     ```
   - Replace `my-nginx` with the name or container ID of your running Nginx container. This command will gracefully stop the container.

2. **Verifying the Container Has Stopped**
   - To ensure the container has stopped, run:
     ```
     docker ps
     ```
   - The stopped container should no longer appear in the list.
   - To see all containers, including stopped ones, run `docker ps -a`.

3. **Removing a Docker Container**
   - Once a container is stopped, you can remove it to free up space. Use the following command:
     ```
     docker rm my-nginx
     ```
   - Again, replace `my-nginx` with the appropriate container name or ID.
   - This command removes the container instance from your system.

4. **Verifying the Container Has Been Removed**
   - To confirm that the container has been successfully removed, run:
     ```
     docker ps -a
     ```
   - The removed container should no longer be listed.

#### Additional Notes
- Stopping a container does not delete the data inside it. You can restart the container later with `docker start`.
- Removing a container deletes it and its data permanently. Ensure you have backed up any necessary data before removing a container.

#### Outcome
By completing this task, you have learned how to stop and remove Docker containers, essential skills for managing Docker resources and maintaining a clean Docker environment.

#### Next Steps
With these skills, you can now manage the lifecycle of Docker containers effectively. You can proceed to more advanced Docker tasks, such as working with Docker volumes or networking.
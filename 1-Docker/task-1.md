### Task 1: Finding and Pulling the Nginx Docker Image

#### Objective
Learn how to find and pull the Nginx image from Docker Hub, the official repository for Docker images.

#### Instructions

1. **Navigate to Docker Hub**
   - Open your web browser and go to [Docker Hub](https://hub.docker.com/).
   - If you don't have a Docker Hub account, consider creating one for additional features, though it's not necessary for this task.

2. **Search for the Nginx Image**
   - In the search bar at the top of the Docker Hub page, type "nginx" and press Enter.
   - You will see a list of available Nginx images. Look for the official Nginx image, usually listed at the top and marked as "official".

3. **Explore the Nginx Image Details**
   - Click on the official Nginx image to view more details.
   - On this page, you can find information about the image, including tags for different versions, last updated date, and the size of the image.
   - Take a moment to read the description and any additional information provided, such as how to use the image, which can be very helpful.

4. **Pull the Nginx Image Using Docker Command**
   - Open your command-line interface (CLI).
   - To pull the latest version of the Nginx image, type the following command and press Enter:
     ```
     docker pull nginx
     ```
   - Docker will download the Nginx image from Docker Hub to your local machine. If you want a specific version, you can use a tag like `nginx:<tag>`, where `<tag>` is the version number.

5. **Verify the Image Pull**
   - After the download is complete, verify that the image is now on your machine by running:
     ```
     docker images
     ```
   - You should see "nginx" listed in your images.

#### Outcome
By completing this task, you will have successfully located and pulled the official Nginx image from Docker Hub, ready for use in subsequent tasks.

#### Next Steps
Proceed to the next task where you will learn to run a Docker container using this Nginx image.
### Task 2: Listing Docker Images

#### Objective
Learn how to list and inspect Docker images on your machine, focusing on understanding the details of the images you have pulled.

#### Instructions

1. **Open Command-Line Interface**
   - Access your command-line interface (CLI). This could be Terminal on macOS and Linux or Command Prompt or PowerShell on Windows.

2. **List Docker Images**
   - To see a list of all Docker images on your machine, enter the following command:
     ```
     docker images
     ```
   - Press Enter. Docker will display a list of images, including the Nginx image you pulled in Task 1.

3. **Understanding the Image List**
   - The output will typically include columns like 'REPOSITORY', 'TAG', 'IMAGE ID', 'CREATED', and 'SIZE'.
   - **REPOSITORY** shows the name of the image (e.g., nginx).
   - **TAG** indicates the version of the image. If you didn't specify a tag during the pull, it will default to 'latest'.
   - **IMAGE ID** is a unique identifier for the image.
   - **CREATED** shows how long ago the image was created.
   - **SIZE** indicates the size of the image.

4. **Inspect a Specific Image (Optional)**
   - If you want more details about a specific image, use the `docker image inspect` command followed by the image name or ID. For example:
     ```
     docker image inspect nginx
     ```
   - This command provides detailed information about the image, including its configuration, layers, and history.

5. **Document or Note Important Details**
   - It might be helpful to note down the IMAGE ID or TAG of the images you plan to use frequently. This information can be useful for future reference, especially when dealing with multiple images or versions.

#### Outcome
By completing this task, you will have gained the ability to list and understand the details of Docker images on your system, an essential skill for Docker image management.

#### Next Steps
Proceed to the next task where you will use the Nginx image to run a Docker container.
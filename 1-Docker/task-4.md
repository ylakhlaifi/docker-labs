### Task 4: Implementing Port Forwarding

#### Objective
Learn how to implement port forwarding in Docker. This task will demonstrate how to expose a Docker container's port to a host machine, allowing external access to the services running inside the container.

#### Instructions

1. **Understanding Port Forwarding in Docker**
   - Port forwarding in Docker involves mapping a port inside a container to a port on the host machine.
   - This is crucial for accessing applications running inside containers from outside the Docker host.

2. **Run a Docker Container with Port Forwarding**
   - You will run an Nginx container and map its port to a port on your host machine.
   - Use the following command:
     ```
     docker run --name my-nginx-port -d -p 8080:80 nginx
     ```
   - Here's the breakdown of the command:
     - `--name my-nginx-port`: Assigns the container a name for easy reference.
     - `-d`: Runs the container in detached mode (in the background).
     - `-p 8080:80`: Maps port 8080 on your host to port 80 on the container. This means that accessing port 8080 on your host will forward requests to port 80 on the container.
     - `nginx`: Specifies the image to use.

3. **Accessing the Nginx Server**
   - Open a web browser and navigate to `http://localhost:8080`.
   - You should see the Nginx welcome page, indicating that the port forwarding is working correctly.

4. **Verifying Port Forwarding**
   - To confirm that the port forwarding is set up correctly, you can run:
     ```
     docker ps
     ```
   - Look for the "PORTS" section in the output. It should show something like `0.0.0.0:8080->80/tcp`, indicating that port 8080 on your host is mapped to port 80 on the container.

#### Outcome
By completing this task, you have successfully set up port forwarding for a Docker container. This allows you to access the Nginx server running inside the container from your host machine.

#### Next Steps
With the ability to forward ports, you can now access various services running in Docker containers from outside the Docker host. This skill is fundamental for deploying and testing web applications using Docker.
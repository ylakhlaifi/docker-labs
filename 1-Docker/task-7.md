### Task 7: Cleanup

#### Objective
Learn how to clean up your Docker environment by removing containers, images, and volumes. This task is essential for maintaining a tidy and efficient Docker setup, especially after testing or development work.

#### Instructions

1. **Stopping Containers**
   - Before removing containers, they need to be stopped. If you have any running containers (like `my-nginx`, `my-nginx-port`, or `my-nginx-volume`), stop them using:
     ```
     docker stop [container_name]
     ```
   - Replace `[container_name]` with the actual name of the container you want to stop.

2. **Removing Containers**
   - Once the containers are stopped, you can remove them. Use the command:
     ```
     docker rm [container_name]
     ```
   - Again, replace `[container_name]` with the name of the container. You can remove multiple containers at once by listing their names separated by spaces.

3. **Removing Images**
   - If you wish to remove the Nginx image, first ensure no containers are using it. Then, use the command:
     ```
     docker rmi nginx
     ```
   - This will remove the Nginx image from your local machine. If you have multiple tags of the Nginx image and want to remove them all, you will need to remove each tag individually.

4. **Cleaning Up Unused Volumes**
   - If you created any volumes during this lab and wish to remove them, first identify unused volumes with:
     ```
     docker volume ls
     ```
   - To remove an unused volume, use:
     ```
     docker volume rm [volume_name]
     ```
   - Replace `[volume_name]` with the name of the volume you want to remove.

5. **Optional: Pruning System-Wide**
   - Docker provides a way to clean up unused containers, networks, images, and optionally, volumes system-wide. Use with caution:
     ```
     docker system prune
     ```
   - To include volumes in the cleanup, use:
     ```
     docker system prune --volumes
     ```

#### Outcome
By completing this task, you have learned how to effectively clean up your Docker environment, which is crucial for managing resources and maintaining system performance.

#### Next Steps
With your Docker environment cleaned up, you can move on to more advanced Docker tasks or experiments, knowing how to maintain a clean setup. This skill is particularly important in environments where Docker is used extensively for development, testing, or production deployments.